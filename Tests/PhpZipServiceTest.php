<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 11/03/2019
 * Time: 08:26
 */

namespace Etshy\Bundle\PhpZipBundle\Tests;


use Etshy\Bundle\PhpZipBundle\Services\PhpZipService;
use PHPUnit\Framework\TestCase;
use PhpZip\ZipFile;

class PhpZipServiceTest extends TestCase
{
    
    /**
     * @var PhpZipService
     */
    private $phpZipService;
    
    public function testCreateZip()
    {
        $this->assertInstanceOf(ZipFile::class, $this->phpZipService->createZip());
    }
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->phpZipService = new PhpZipService();
    }
}

<?php
declare(strict_types=1);

namespace Etshy\Bundle\PhpZipBundle\Services;


use PhpZip\ZipFile;

class PhpZipService implements PhpZipInterface
{

    private ZipFile $zip;

    /**
     * @return ZipFile
     */
    public function createZip(): ZipFile
    {
        return $this->zip = new ZipFile();
    }

}
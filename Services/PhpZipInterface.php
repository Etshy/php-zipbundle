<?php
declare(strict_types=1);

namespace Etshy\Bundle\PhpZipBundle\Services;


use PhpZip\ZipFile;

interface PhpZipInterface
{
    public function createZip(): ZipFile;
    
}